///enable_movement_run(acceleration, max_speed, xaxis, yaxis)
/*
    Call this script in order to enable
    running on a movement entity.
*/

var acc = argument[0]; // Acceleration value
var maxspd = argument[1]; // Maximum run speed
var xaxis = argument[2]; // Right movement input
var yaxis = argument[3]; // Left movement input

horizontal_move_input = xaxis != 0;
vertical_move_input = yaxis != 0;

if (xaxis == 0 && yaxis == 0) exit;


var dir = point_direction(0, 0, xaxis, yaxis);
add_movement_direction_acceleration_maxspeed(dir, acc, maxspd);
